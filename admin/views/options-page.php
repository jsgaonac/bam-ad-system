<?php

use AdSystem\Helpers\DB;

if (!current_user_can('manage_options')) {
    wp_die('User is not authorized');
}

if (isset($_POST['_wpnonce'])) {
    check_admin_referer('ad_system_options_page_nonce');

    $title = $_POST['title'];
    $type = strtolower($_POST['type']);
    $template = strtolower($_POST['template']);
    $date = $_POST['date'];

    if (isset($_POST['delete'])) {
        DB::deleteAd($_POST['id']);
    } elseif (isset($_POST['update'])) {
        DB::updateAd($_POST['id'], $title, $type, $template, $date);
    } else {
        DB::insertAd($title, $type, $template, $date);
    }
}

// Get the markup for the form used to create ads
require_once('partials/create-ad-form.php');

// We're not editing an ad by default
$currentAd = null;

if (isset($_GET['id'])) {
    $currentAd = DB::getAd($_GET['id']);
}

// Get the markup for the form used to edit ads
require_once('partials/edit-ad-form.php');

$ads = DB::getAllAds();
$optionsPageUrl = $_SERVER['REQUEST_URI'];
?>

<h2>Existing Ads</h2>

<?php if (count($ads)): ?>
<div class="ads-container">
    <?php foreach ($ads as $ad): ?>
        <div class="ad-admin">
            <div class="title">Title: <?= $ad->title; ?></div>
            <div class="type">Type: <?= ucfirst($ad->type); ?></div>
            <div class="template">Template: <?= ucfirst($ad->template); ?></div>
            <?php if ($ad->type === 'pick'): ?>
                <div class="datetime">Date: <?= $ad->date ?></div>
            <?php endif; ?>

            <div>
                <div class="label-text">Shortcode:</div>
                <input type="text" value="<?= "[ad id='$ad->id']" ?>">
            </div>

            <a class="edit-link action-button" href="<?= add_query_arg('id', $ad->id, $optionsPageUrl) ?>">Edit Ad</a>
        </div>
    <?php endforeach; ?>
</div>
<?php else: ?>
    <div>There are no ads</div>
<?php endif; ?>
