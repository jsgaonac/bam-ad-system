<h2>Create New Ad</h2>

<form method="post">
    <div class="input-group">
        <label for="title">Title</label>
        <input type="text" id="title" name="title" required>
    </div>

    <div class="input-group">
        <label for="type-select">Type</label>
        <select class="type-field" name="type" id="type-select">
            <option value="regular" default>Regular</option>
            <option value="pick">Pick</option>
        </select>
    </div>

    <div class="input-group">
        <label for="template-select">Template</label>
        <select name="template" id="template-select">
            <option value="center" default>Center</option>
            <option value="left">Left</option>
            <option value="right">Right</option>
        </select>
    </div>

    <div class="input-group hidden date-picker-container">
        <label for="date">Date</label>
        <input id="date-picker" type="datetime" name="date" id="date">
    </div>

    <?php wp_nonce_field('ad_system_options_page_nonce'); ?>

    <button class="action-button" type="submit">Create</button>
</form>
