<?php if ($currentAd): ?>
<h2>Editing Ad</h2>

<form method="post" onsubmit="return confirm('Do you want to continue?');">
    <input type="hidden" name="id" value="<?= $currentAd->id ?>">

    <div class="input-group">
        <label for="title-edit">Title</label>
        <input type="text" id="title-edit" name="title" value="<?= $currentAd->title ?>" required>
    </div>

    <div class="input-group">
        <label for="type-select-edit">Type</label>
        <select class="type-field" name="type" id="type-select-edit">
            <option value="regular" <?= $currentAd->type === 'regular' ? 'selected' : ''; ?>>Regular</option>
            <option value="pick" <?= $currentAd->type === 'pick' ? 'selected' : ''; ?>>Pick</option>
        </select>
    </div>

    <div class="input-group">
        <label for="template-select-edit">Template</label>
        <select name="template" id="template-select-edit">
            <option value="center" <?= $currentAd->template === 'center' ? 'selected' : ''; ?>>Center</option>
            <option value="left" <?= $currentAd->template === 'left' ? 'selected' : ''; ?>>Left</option>
            <option value="right" <?= $currentAd->template === 'right' ? 'selected' : ''; ?>>Right</option>
        </select>
    </div>

    <div class="input-group <?= $currentAd->type !== 'pick' ? 'hidden' : '' ?> date-picker-container">
        <label for="date">Date</label>
        <input id="date-picker-edit" type="datetime" name="date" id="date" value="<?= $currentAd->date ?>">
    </div>

    <?php wp_nonce_field('ad_system_options_page_nonce'); ?>

        <input class="action-button" type="submit" name="update" value="Update">
        <input class="action-button action-button-alt" type="submit" name="delete" value="Delete">
</form>
<?php endif; ?>
