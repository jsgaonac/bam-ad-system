import postcss from 'rollup-plugin-postcss';
import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
  input: 'src/js/main.js',
  output: {
    file: 'public/js/scripts.js',
    format: 'iife'
  },
  plugins: [
    postcss({
      extensions: ['.css', '.scss'],
      extract: 'public/css/styles.css',
      minimize: true,
    }),

    resolve(),
    commonjs(),

    babel({
      exclude: 'node_modules/**'
    }),
  ],
};
