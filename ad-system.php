<?php

/**
 * Plugin Name: Ad System
 * Description: Plugin for inserting ads in posts
 * Version:     1.0.0
 * Author:      Juan S. Gaona C.
 * Text Domain: ad-system
 * License:     GPL2
 */

define('AD_SYSTEM_PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once 'autoload.php';

use AdSystem\Bootstrap;
use AdSystem\AdSystem;

/**
 * Called when the plugin is activated
 *
 * @return void
 */
function adSystemOnActivation()
{
    Bootstrap::onActivation();
}

register_activation_hook(__FILE__, 'adSystemOnActivation');

/**
 * Called when the plugin is deactivated
 *
 * @return void
 */
function adSystemOnDeactivation()
{
    Bootstrap::onDeactivation();
}

register_deactivation_hook(__FILE__, 'adSystemOnDeactivation');

// We get the instance of the AdSystem plugin and start it.
$adSystemInstance = AdSystem::getInstance();
$adSystemInstance->start();
