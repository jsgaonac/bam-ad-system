<?php

namespace AdSystem\Helpers;

/**
 * Abstracts DB operations.
 */
class DB
{
    public static function createTable()
    {
        global $wpdb;

        $charsetCollate = $wpdb->get_charset_collate();
        $tableName = self::getTableName();

        $sql = "CREATE TABLE $tableName (
            id int(10) NOT NULL AUTO_INCREMENT,
            title varchar(70) NOT NULL,
            type varchar(25) NOT NULL,
            template varchar(25) NOT NULL,
            date datetime,
            PRIMARY KEY  (id)
        ) $charsetCollate;";

        require_once(ABSPATH.'wp-admin/includes/upgrade.php');

        dbDelta($sql);
    }

    public static function getAllAds()
    {
        global $wpdb;

        return $wpdb->get_results('SELECT * FROM '.self::getTableName());
    }

    public static function insertAd($title, $type, $template, $date)
    {
        global $wpdb;

        $wpdb->insert(
            self::getTableName(),
            [
                'title' => $title,
                'type' => $type,
                'template' => $template,
                'date' => $date,
            ],
            [
                '%s',
                '%s',
                '%s',
                '%s'
            ]
        );
    }

    public static function getAd($id)
    {
        global $wpdb;

        return $wpdb->get_row('SELECT * FROM '.self::getTableName().' WHERE id = '.$id);
    }

    public static function updateAd($id, $title, $type, $template, $date)
    {
        global $wpdb;

        $wpdb->update(
            self::getTableName(),
            [
                'title' => $title,
                'type' => $type,
                'template' => $template,
                'date' => $date,
            ],
            [
                'id' => $id
            ],
            [
                '%s',
                '%s',
                '%s',
                '%s',
            ],
            [
                '%d'
            ]
        );
    }

    public static function deleteAd($id)
    {
        global $wpdb;

        $wpdb->delete(self::getTableName(), ['id' => $id], ['%d']);
    }

    private static function getTableName()
    {
        global $wpdb;

        return $wpdb->prefix.'ad_system_ads';
    }
}
