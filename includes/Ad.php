<?php

namespace AdSystem;

use AdSystem\Helpers\DB;

/**
 * Handles the shortcut logic
 */
class Ad
{
    public static function renderAd($attributes)
    {
        if (!isset($attributes['id'])) {
            return '';
        }

        $ad = DB::getAd($attributes['id']);

        if (!$ad) {
            return '';
        }

        $id = $ad->id;
        $title = $ad->title;
        $template = $ad->template;
        $type = $ad->type;
        $dateTime = $ad->date;
        $categories = implode(' ', array_reduce(get_the_category(), function($acc, $cat) {
            $acc[] = $cat->slug;

            return $acc;
        }, []));

        return "
            <div data-type='$type' class='ad-elem ad-elem-$id $template $categories'>
                <div class='title'>$title</div>
                <div class='countdown hidden' data-time='$dateTime'></div>
            </div>
        ";
    }
}
