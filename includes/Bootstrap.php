<?php

namespace AdSystem;

use AdSystem\Helpers\DB;

/**
 * Plugin bootstrapping functionality
 */
class Bootstrap
{
    public static function onActivation()
    {
        // We create a table to hold the Ads data
        DB::createTable();
    }

    public static function onDeactivation()
    {
        // Remove the options page on deactivation
        add_action('admin_menu', ['AdSystem\Settings', 'removeOptionsMenu'], 99);
    }
}
