<?php

namespace AdSystem;

/**
 * Handles the menu page logic
 */
class Settings
{
    private const OPTIONS_VIEW = AD_SYSTEM_PLUGIN_DIR.'/admin/views/options-page.php';

    public static function addOptionsPage()
    {
        add_menu_page('Ads', 'Ads options', 'manage_options', self::OPTIONS_VIEW, null, 'dashicons-excerpt-view', 26);
    }

    public static function removeOptionsPage()
    {
        remove_menu_page(self::OPTIONS_VIEW);
    }
}
