<?php

namespace AdSystem;

/**
 * Handles the plugin resources
 */
class AdSystem
{
    protected static $instance = null;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new AdSystem();
        }

        return self::$instance;
    }

    public function start()
    {
        $this->loadHooks();
    }

    private function loadHooks()
    {
        $this->loadActions();
        $this->loadResources();
        $this->loadShortcodes();
    }

    private function loadActions()
    {
        add_action('admin_menu', ['AdSystem\Settings', 'addOptionsPage']);
    }

    private static function loadResources()
    {
        add_action('wp_enqueue_scripts', ['AdSystem\AdSystem', 'enqueueResources']);
        add_action('admin_enqueue_scripts', ['AdSystem\AdSystem', 'enqueueResources']);
    }

    public static function enqueueResources()
    {
        $pluginDir = plugin_dir_url(__FILE__).'../public';

        wp_enqueue_style('ad-system-styles', "$pluginDir/css/styles.css", [], null);
        wp_enqueue_script('ad-system-scripts', "$pluginDir/js/scripts.js", [], null, true);
    }

    private function loadShortcodes()
    {
        add_shortcode('ad', ['AdSystem\Ad', 'renderAd']);
    }

    private function __construct()
    {
        // Do nothing.
    }
}
