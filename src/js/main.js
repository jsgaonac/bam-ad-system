import '../scss/main.scss';
import flatpickr from 'flatpickr';
import countdown from 'light-countdown';

const setDatePicker = function() {
  const flatpickrConfig = {
    enableTime: true,
    dateFormat: 'Y-m-d H:i',
    minDate: 'today',
  };

  new flatpickr('#date-picker', flatpickrConfig);
  new flatpickr('#date-picker-edit', flatpickrConfig);
};

const setDatePickerFields = function() {
  let selects = Array.from(document.querySelectorAll('form select.type-field'));

  selects.forEach((sl) => {
    sl.addEventListener('change', (e) => {
      const formElems = Array.from(document.querySelectorAll('form'));

      formElems.forEach((form) => {
        if (form.contains(e.target)) {
          const pickerContainer = form.querySelector('.date-picker-container');

          if (e.target.value === 'pick') {
            pickerContainer.classList.remove('hidden');
          } else {
            pickerContainer.classList.add('hidden');
          }
        }
      });
    });
  });
};

const setCountdown = function() {
  const countDownElems = Array.from(document.querySelectorAll('.ad-elem[data-type="pick"]'));

  countDownElems.forEach((it) => {
    const countElem = it.querySelector('.countdown');
    countElem.classList.remove('hidden');

    countdown({
      timeEnd: Date.parse(countElem.dataset.time),
      msgPattern: `
        <span class="countdown-square">
          {days}
          <span>days</span>
        </span>

        <span class="countdown-square">
          {hours}
          <span>hours</span>
        </span>

        <span class="countdown-square">
          {minutes}
          <span>minutes</span>
        </span>

        <span class="countdown-square">
          {seconds}
          <span>seconds</span>
        </span>`,
      selector: `.${it.classList.value.split(' ').join('.')} .countdown`,
    });
  });
};

document.addEventListener('DOMContentLoaded', () => {
  setDatePickerFields();
  setDatePicker();
  setCountdown();
});
