<?php

/**
 * PSR-4 compliant autoloader
 */

spl_autoload_register(function($class) {
    $namespacePrefix = 'AdSystem\\';
    $baseDir = __DIR__ . '/includes';

    $prefixLength = strlen($namespacePrefix);

    if (strncmp($namespacePrefix, $class, $prefixLength) !== 0) {
        return;
    }

    $classWithoutPrefix = substr($class, $prefixLength);
    $classFile = $baseDir.'/'.str_replace('\\', '/', $classWithoutPrefix).'.php';

    if (file_exists($classFile)) {
        require_once $classFile;
    }
});
